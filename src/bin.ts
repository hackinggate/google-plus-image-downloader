const { google } = require('googleapis');
import mkdirp = require('mkdirp')
import fs = require('fs')
import request = require('request')
import readlineSync = require('readline-sync');
var d = new Date();
var currentDatetime = d.toISOString();
var isFirstLine = true
var userId = ''
var isCopyUrlOnly = false
const plus = google.plus({
  version: 'v1',
  auth: process.env.googlePlusApiKey || 'YOUR_API_KEY' // specify your API key here
});
main()
function main() {
  console.log("=== Google Plus Image Downloader === ")
  var input = readlineSync.question("\nPlease enter user id: ");
  console.log()
  var inputCopyUrlOnly = readlineSync.question("Do you want save url list only? (image will not be download) [y/N] ");
  if (inputCopyUrlOnly.toLowerCase() == 'y')
    isCopyUrlOnly = true
  if (input) {
    userId = input
    dlTask(undefined).catch(console.error);
  } else {
    console.log("invalid input, exit.")
  }


}

async function dlTask(nextToken) {
  let options = {
    userId: userId,
    collection: 'public'
  }
  if (nextToken && nextToken.length > 0)
    options['pageToken'] = nextToken
  const res1 = await plus.activities.list(options);
  var res = res1.data
  var items = res.items || undefined


  for (let j in items) {
    var item = items[j]

    if (item && item.published && item.object && item.object.attachments && item.object.attachments[0]) {

      console.log(item.object.attachments[0])
      let attachment = item.object.attachments[0]
      let objectType = `${attachment.objectType}` || undefined
      if (objectType == 'photo') {
        imageUrlInserter(`${attachment.fullImage.url}` || undefined, item.published)
      } else if (objectType == 'album') {
        for (let i in attachment.thumbnails) {
          imageUrlInserter(`${attachment.thumbnails[i].image.url}` || undefined, item.published)
        }
      } else {
        imageUrlInserter(undefined, item.published)
      }

    }
  }
  if (res.nextPageToken) {
    await dlTask(res.nextPageToken)
  }
}
function thumbnailUrlToRawUrl(thumbnailUrl) {
  var short = thumbnailUrl.search(/\/\w\d+(-\w\d*)-rw\//);
  var long = thumbnailUrl.search(/\/\w\d+(-\w\d*)-[a-zA-Z]-rw\//);
  var mid = thumbnailUrl.search(/\/\w\d+(-\w\d*)-[a-zA-Z]\//);
  var simple = thumbnailUrl.search(/\/\w\d+(-\w\d*)\//);

  var header = thumbnailUrl.search(/https:\/\/lh[0-9]+.googleusercontent.com/g);

  var newUrl = thumbnailUrl;

  if (short != -1 && header != -1) {
    newUrl = thumbnailUrl.replace(/\/\w\d+(-\w\d*)-rw\//, '/s0/');
  } else if (long != -1 && header != -1) {
    newUrl = thumbnailUrl.replace(/\/\w\d+(-\w\d*)-[a-zA-Z]-rw\//, '/s0/');
  } else if (mid != -1 && header != -1) {
    newUrl = thumbnailUrl.replace(/\/\w\d+(-\w\d*)-[a-zA-Z]\//, '/s0/');
  } else if (simple != -1 && header != -1) {
    newUrl = thumbnailUrl.replace(/\/\w\d+(-\w\d*)\//, '/s0/');
  }
  return newUrl;

}

function imageUrlInserter(imageUrl, publishedDate) {
  if (imageUrl) {
    writeToUrlTxtFile(thumbnailUrlToRawUrl(imageUrl) + " " + publishedDate)
    if (!isCopyUrlOnly) {
      copyImageFromUrl(imageUrl, publishedDate)
    }
  } else {
    console.log('invalid image url, skiped.')
  }
}
function downloadImage(url, path, publishedDate, callback) {
  request.head(url, function(err, res, body) {
    let contentType = String(res.headers['content-type']);
    let contentLength = String(res.headers['content-length']);
    console.log('content-type:', contentType);
    console.log('content-length:', contentLength);

    let mimesToExt = {
      'image/jpeg': 'jpg',
      'image/png': 'png',
      'image/gif': 'gif'
    }
    var filepath = path + publishedDate + "+" + guid();
    let ext = mimesToExt[contentType];
    if (ext) {
       filepath += `.${mimesToExt[contentType]}`
    }

    request(url).pipe(fs.createWriteStream(filepath)).on('close', callback);
  });
}

function copyImageFromUrl(url, publishedDate) {
  var n = url.substring(url.lastIndexOf('/') + 1)
  var raw = n.split('.')
  var rawname = raw[0]
  var rawFormat = raw[1]
  var path = __dirname + `/../output/images/${currentDatetime}/`
  mkdirp(path, function(err) {
    if (err)
      console.log(err);
    downloadImage(url, path, publishedDate, function() {
      console.log('done');
    });
  });
}
function writeToUrlTxtFile(text) {
  var path = __dirname + "/../output/urls/"
  var filepath = path + `imgUrl_export_${currentDatetime}.txt`
  console.log(`found new content: \n${text}\n writing`)
  mkdirp(path, function(err) {
    if (err)
      console.log(err);
    let writeText = text
    if (isFirstLine) {
      writeText = text
      isFirstLine = false
    } else {
      writeText = `\n${text}`
    }
    fs.appendFile(filepath, writeText, (err) => console.log(err))
  });
}
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
